import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})


export class CalculatorComponent implements OnInit {
  display = '';
  lastKey = '';
  constructor() { }

  ngOnInit() {
  }
  process(event) {
    const input = (event.target ||
                    event.srcElement ||
                    event.currentTarget).textContent;
    if (this.display === 'Overflow') {
      this.display = '';
      this.lastKey = '';
    }
    switch (input) {
      case 'CLEAR':
        this.display = '';
        this.lastKey = '';
        break;
      case '=':
        this.display = '' + eval(this.display);
        if (this.display.length > 15) {
          this.display = this.display.slice(0, 14);
        }
        break;
      case '%':
      case '/':
      case '*':
      case '+':
      case '-':
      case '.':
        if (['%', '/', '*', '+', '-', '.'].indexOf(this.lastKey) !== -1) {
        } else if (this.display.length < 14) {
          this.display += input;
        }
        break;
      default:
        if (this.display.length < 14) {
          this.display += input;
        }
        break;
      }
    }
  }
