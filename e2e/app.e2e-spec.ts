import { TributePagePage } from './app.po';

describe('tribute-page App', () => {
  let page: TributePagePage;

  beforeEach(() => {
    page = new TributePagePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
